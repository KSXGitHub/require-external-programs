export enum ExitStatusCodes {
  Satisfied = 0,
  Unsatisfied = 1,
  CaughtException = 2,
  UncaughtException = -1
}
